//
//  Font.swift
//  cleaning-service
//
//  Created by Mr Zee on 11/21/20.
//

import Foundation
import UIKit

extension UIFont {
    
    public enum app: String {
        
        public func value(of size: CGFloat) -> UIFont? {
            return UIFont.init(name: self.rawValue, size: size)
        }
        
        case test
        
    }
    
}

