//
//  Settings.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation


class Settings: NSObject {
    
    enum Settings_App: String {
        case test
    }
    
    private let userDefault = UserDefaults.standard
    
    
    
    public func setting_load(for key: Settings_App) -> Any? {
        return userDefault.value(forKey: key.rawValue)
    }
    
    public func setting_save(_ value: Any?, for key: Settings_App) {
        userDefault.setValue(value, forKey: key.rawValue)
    }
    
    public func setting_delete(for key: Settings_App) {
        userDefault.removeObject(forKey: key.rawValue)
    }
    
}
