//
//  DataSource.swift
//  DadeAfarinTest
//
//  Created by MohammadReza Zamanieh on 8/24/21.
//

import Foundation
import UIKit

class DataSource: NSObject {
    
    
    // MARK: - CATEGORIES
    public static var cat1: Model.category = .init(id: 0, name: "Item 1", image: UIImage.app.cat1.rawValue)
    public static var cat2: Model.category = .init(id: 1, name: "Item 2", image: UIImage.app.cat2.rawValue)
    public static var cat3: Model.category = .init(id: 2, name: "Item 3", image: UIImage.app.cat3.rawValue)
    public static var cat4: Model.category = .init(id: 3, name: "Item 4", image: UIImage.app.cat4.rawValue)
    public static var cat5: Model.category = .init(id: 4, name: "Item 5", image: UIImage.app.cat1.rawValue)
    public static var cat6: Model.category = .init(id: 5, name: "Item 6", image: UIImage.app.cat2.rawValue)
    public static var cat7: Model.category = .init(id: 6, name: "Item 7", image: UIImage.app.cat3.rawValue)
    public static var cat8: Model.category = .init(id: 7, name: "Item 8", image: UIImage.app.cat4.rawValue)
    public static var cat9: Model.category = .init(id: 8, name: "Item 9", image: UIImage.app.cat1.rawValue)
    
    
    // MARK: - PRODUCTS
    public var pro1: Model.product = .init(id: 0, name: "product 1", image: UIImage.app.pro1.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    public var pro2: Model.product = .init(id: 1, name: "product 2", image: UIImage.app.pro2.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    public var pro3: Model.product = .init(id: 2, name: "product 3", image: UIImage.app.pro3.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    public var pro4: Model.product = .init(id: 3, name: "product 4", image: UIImage.app.pro4.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    public var pro5: Model.product = .init(id: 4, name: "product 5", image: UIImage.app.pro5.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    public var pro6: Model.product = .init(id: 5, name: "product 6", image: UIImage.app.pro6.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    public var pro7: Model.product = .init(id: 6, name: "product 7", image: UIImage.app.pro7.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    public var pro8: Model.product = .init(id: 7, name: "product 8", image: UIImage.app.pro1.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    public var pro9: Model.product = .init(id: 8, name: "product 9", image: UIImage.app.pro2.rawValue, description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", category: DataSource.cat1)
    
    
}
