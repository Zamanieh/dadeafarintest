//
//  CartManager.swift
//  Lilas
//
//  Created by Mr.Zee on 4/16/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay


class CartManager: NSObject {
    
    public var items: BehaviorRelay<[Model.order]> = BehaviorRelay.init(value: [])
    private let manager = FileManager.default
    private var timer: Timer!
    
    override init() {
        super.init()
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(timerFired(_:)), userInfo: nil, repeats: true)
        timer.tolerance = 1
        RunLoop.main.add(timer, forMode: .default)
    }
    
    public func newId() -> Int {
        let items = self.retrieve() ?? []
        if let id = items.last?.id {
            return id + 1
        } else {
            return 0
        }
    }
    
    public func add(new order: Model.order) {
        self.add([order])
    }
    
    public func add(_ order: [Model.order]) {
        var model = retrieve() ?? []
        for item in order {
            model.append(item)
        }
        let encoded = encode(model)
        if let e = encoded { writeToFile(e)}
        items.accept(model)
    }
    
    public func edit(_ order: Model.order, status: Int) {
        if status > 3 { return }
        let model = retrieve() ?? []
        let find = model.first(where: {$0.id == order.id})
        if var item = find {
            item.status = status
            replace(order, with: item)
        }
    }
    
    public func replace(_ order: [Model.order]) {
        let encoded = encode(order)
        if let e = encoded { writeToFile(e) }
        self.items.accept(order)
    }
    
    public func replace(_ oldItem: Model.order, with newItem: Model.order) {
        var model = retrieve() ?? []
        let index = model.firstIndex(where: { $0.id == oldItem.id })
        if let i = index {
            model[i] = newItem
            let encoded = encode(model)
            if let e = encoded { writeToFile(e) }
            self.items.accept(model)
        }
    }
    
    public func retrieve() -> [Model.order]? {
        let str = getFileContent()
        var model: [Model.order] = []
        if let s = str {
            let decoded = decode(s)
            if let d = decoded { model = d }
        }
        self.items.accept(model)
        return model
    }
    
    public func remove(_ product: Model.order) {
        var model = retrieve() ?? []
        if let i = model.firstIndex(where: {$0.id == product.id}) {
            model.remove(at: i)
        }
        self.replace(model)
    }
    
    public func removeAll() {
        self.items.accept([])
        writeToFile("")
    }
    
    private func directory() -> URL {
        let path = manager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return path.appendingPathComponent("cart.txt")
    }
    
    private func getFileContent() -> String? {
        do {
            return try String(contentsOf: directory(), encoding: .utf8)
        } catch (let err) {
            debugPrint(err)
            return nil
        }
    }
    
    @discardableResult private func writeToFile(_ str: String) -> Bool {
        let emptyStr = ""
        do {
            try emptyStr.write(to: directory(), atomically: true, encoding: .utf8)
            try str.write(to: directory(), atomically: true, encoding: .utf8)
            return true
        } catch (let err) {
            debugPrint(err)
            return false
        }
    }
    
    private func decode(_ str: String) -> [Model.order]? {
        do {
            return try JSONDecoder().decode([Model.order].self, from: str.data(using: .utf8)!)
        } catch (let err) {
            debugPrint(err)
            return nil
        }
    }
    
    private func encode(_ Cart_Model: [Model.order]) -> String? {
        do {
            let data = try JSONEncoder().encode(Cart_Model)
            return String(data: data, encoding: .utf8)
        } catch (let err) {
            debugPrint(err)
            return nil
        }
    }
    
    // MARK: - TIMER FUNC
    @objc private func timerFired(_ sender: Timer) {
        let items = self.retrieve() ?? []
        for item in items {
            if item.status < 3 {
                var new = item
                new.status += 1
                self.replace(item, with: new)
            }
        }
    }
    
}
