//
//  ZCollectionViewCell.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 7/31/21.
//

import Foundation
import UIKit

fileprivate var reuseIdentifierKey = "reuseIdentifier"

class ZCollectionViewCell: UICollectionViewCell {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// in order to use ZCollectionViewcell you should override this method
    open func initialize() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }
    
    public static func reuseIdentifier() -> String {
        var id = self.init().meta(for: reuseIdentifierKey) as? String
        if id == nil {
            id = NSStringFromClass(Self.self)
            self.init().set(meta: id!, for: reuseIdentifierKey)
        }
        return id!
    }
}
