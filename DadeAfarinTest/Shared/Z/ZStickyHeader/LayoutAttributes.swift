//
//  ZStickyHeaderLayoutAttributes.swift
//  Unite
//
//  Created by Mr Zee on 4/21/21.
//

import UIKit

extension ZStickyHeader.CollectionView {
    
    public class LayoutAttributes: UICollectionViewLayoutAttributes {
        
        public var collapseProgress: CGFloat = 0
        public var overOffsetAmount: CGFloat = 0
        public var headerSize: HeaderSize = .init(minimumHeight: 0, normalHeight: 0)
        public var progress: CGFloat = 0
        
        override public func copy(with zone: NSZone? = nil) -> Any {
            let copy = super.copy(with: zone) as! LayoutAttributes
            copy.collapseProgress = collapseProgress
            copy.overOffsetAmount = overOffsetAmount
            copy.headerSize = headerSize
            copy.progress = progress
            return copy
        }
        
        override public func isEqual(_ object: Any?) -> Bool {
            if let obj = object as? LayoutAttributes {
                return super.isEqual(obj) && collapseProgress.isEqual(to: obj.collapseProgress) && overOffsetAmount.isEqual(to: obj.overOffsetAmount) && progress.isEqual(to: obj.progress)
            }
            return super.isEqual(object)
        }
        
    }
    
    public struct HeaderSize {
        public var minimumHeight: CGFloat
        public var normalHeight: CGFloat
    }
    
}
