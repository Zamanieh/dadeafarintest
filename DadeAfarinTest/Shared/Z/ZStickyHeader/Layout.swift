//
//  ZStickyHeader + CollectionView.swift
//  Unite
//
//  Created by Mr Zee on 4/21/21.
//

import Foundation
import UIKit
import simd

public protocol ZStickyHeaderCollectionViewFlowLayoutDelegate: NSObject {
    func headerInstanceFor(layout: ZStickyHeader.CollectionView.FlowLayout) -> UICollectionReusableView?
    func headerSize(layout: ZStickyHeader.CollectionView.FlowLayout, collectionViewSize: CGSize) -> ZStickyHeader.CollectionView.HeaderSize
}

extension ZStickyHeaderCollectionViewFlowLayoutDelegate {
    public func headerInstanceFor(layout: ZStickyHeader.CollectionView.FlowLayout) -> UICollectionReusableView? {
        nil
    }
    
    public func headerSize(layout: ZStickyHeader.CollectionView.FlowLayout, collectionViewSize: CGSize) -> ZStickyHeader.CollectionView.HeaderSize {
        return .init(minimumHeight: UINavigationController.navBarHeight, normalHeight: 200)
    }
}

extension ZStickyHeader.CollectionView {
    
    
    public class FlowLayout: UICollectionViewFlowLayout {
        
        private let indexPath: IndexPath = .init(row: 0, section: 0)
        public lazy var headerAttr: LayoutAttributes = { LayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, with: indexPath) }()
        public weak var delegate: ZStickyHeaderCollectionViewFlowLayoutDelegate?
        public var sectionPadding: UIEdgeInsets?
        
        /// If you set this to true, you can use NZStickyHeader just as you use UICollectionViewFlowLayout. Useful for polymorphisms.
        public var stickyHeaderEnabled: Bool = true {
            didSet {
                invalidateLayout()
            }
        }
        
        public var strechOnOverScrolling: Bool = true
        /// Optionally override this method if you don't want the NZStickyHeaderCollectionViewLayout to extrude the header on over scrolling. by default, the header becomes bigger if the user scrolls towards to top of the collectionView.
        open func shouldStrechOnOverScrolling() -> Bool {
            strechOnOverScrolling
        }
        
        public var adjustContentSizeToMakeHeaderCollabsible: Bool = true
        /// Optionally override this method if you don't want NZStickyHeaderCollectionViewLayout to force adjust the contentSize of the cv to make the header collapsible even if there's not enough items to make the cv scrollable.
        open func shouldAdjustContentSizeToMakeHeaderCollabsible() -> Bool {
            adjustContentSizeToMakeHeaderCollabsible
        }
        
        public var headerSize: HeaderSize = .init(minimumHeight: UINavigationController.navBarHeight, normalHeight: 200)
        /// Override this method and return the expanded size for the header. by default, the header is expanded initially.
        /// - Parameter collectionViewSize: If your expanded size depends on the size of the collectionview, you can use this parameter
        open func headerSize(collectionViewSize: CGSize) -> HeaderSize {
            delegate?.headerSize(layout: self, collectionViewSize: collectionViewSize) ?? headerSize
        }
        
        /// Override this method and return the living instance of the header you dequeud in your collectionView's controller. To avoid the header glitch during performBatchUpdates().
        open func headerInstance() -> UICollectionReusableView? {
            return delegate?.headerInstanceFor(layout: self)
        }
        
        
        override public func prepare() {
            super.prepare()
            guard let cv = collectionView else { return }
            let w = cv.bounds.width
            let scrollOffset = cv.contentOffset.y + cv.adjustedContentInset.top
            let headerSize = self.headerSize(collectionViewSize: cv.bounds.size)
            let headerSqueezeAmount = headerSize.normalHeight - headerSize.minimumHeight;
            
            sectionInset = sectionPadding ?? .zero
            sectionInset.top += headerSize.normalHeight
            
            let headerSqueeze = max(min(scrollOffset, headerSqueezeAmount), 0)
            let headerHeight = headerSize.normalHeight - headerSqueeze
            
            let collapseProgress = headerSqueeze / headerSqueezeAmount
            
            headerAttr.collapseProgress = collapseProgress
            headerAttr.headerSize = headerSize
            
            if scrollOffset >= 0 {
                headerAttr.frame = CGRect(x: 0, y: scrollOffset + cv.contentInset.top, width: w, height: CGFloat(headerHeight))
                headerAttr.overOffsetAmount = 0
            } else {
                if (shouldStrechOnOverScrolling()) {
                    headerAttr.frame = CGRect(x: 0, y: scrollOffset + cv.contentInset.top, width: w, height: headerSize.normalHeight + abs(scrollOffset))
                    headerAttr.overOffsetAmount = abs(scrollOffset)
                } else {
                    headerAttr.frame = CGRect(x: 0, y: cv.contentInset.top, width: w, height: headerSize.normalHeight)
                    headerAttr.overOffsetAmount = abs(scrollOffset)
                }
            }
            
            if scrollOffset < 0 {
                headerAttr.overOffsetAmount = abs(scrollOffset)
            } else {
                headerAttr.overOffsetAmount = 0
            }
            
            cv.scrollIndicatorInsets = .init(top: headerAttr.frame.height + headerAttr.overOffsetAmount, left: 0, bottom: 0, right: 0)
            
            DispatchQueue.main.async {
                self.headerInstance()?.frame = self.headerAttr.frame
            }
        }
        
        open override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
            var result = super.layoutAttributesForElements(in: rect)
            if result != nil {
                if stickyHeaderEnabled {
                    result!.append(headerAttr)
                }
            }
            return result
        }
        
        override open var collectionViewContentSize: CGSize {
            get { return calculateFinalContentSize() }
        }
        
        internal func calculateFinalContentSize() -> CGSize {
            if shouldAdjustContentSizeToMakeHeaderCollabsible() {
                guard let cv = collectionView else { return super.collectionViewContentSize }
                let minHeight2MakeHeaderCollapsible = cv.bounds.height + headerSize(collectionViewSize: cv.bounds.size).normalHeight - headerSize(collectionViewSize: cv.bounds.size).minimumHeight - cv.adjustedContentInset.top - cv.adjustedContentInset.bottom
                let height = max(super.collectionViewContentSize.height, minHeight2MakeHeaderCollapsible)
                return CGSize(width: super.collectionViewContentSize.width, height: height)
            } else { return collectionViewContentSize }
        }
        
        /// By default, NZStickyHeaderCollectionViewLayout sets the CollectionView's deceleration rate to fast during the collapse of the header. If you don't want this behavior or you want to choose different deceleration rates for normal state and during header collapse state, override decelerationRateDuringHeaderCollapse() and decelerationRateForWhenHeaderIsCollapsed() methods.
        open func decelerationRateDuringHeaderCollapse() -> UIScrollView.DecelerationRate {
            .fast
        }
        
        open func decelerationRateForWhenHeaderIsCollapsed() -> UIScrollView.DecelerationRate {
            .normal
        }
        
        override final public func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
            true
        }
        
        override open func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
            guard let cv = self.collectionView else { return proposedContentOffset }
            let headerSize = self.headerSize(collectionViewSize: cv.bounds.size)
            let headerSqueezeAmount = headerSize.normalHeight - headerSize.minimumHeight
            let headerSqueeze = max(min(proposedContentOffset.y + cv.contentInset.top, headerSqueezeAmount), 0)
            let headerSqueezeProgress = headerSqueeze / headerSqueezeAmount
            
            // target progress will be somewhere between 0 and 1, so we should change the targetContentOffset
            if headerSqueezeProgress > 0 && headerSqueezeProgress < 1.0 {
                collectionView?.decelerationRate = .fast
                var result = CGPoint()
                if (headerSqueezeProgress >= 0.5) {
                    result.y = -cv.adjustedContentInset.top + headerSqueezeAmount
                } else {
                    result.y = -cv.adjustedContentInset.top
                }
                
                let decelerationDir = sign(Double(velocity.y))

                let proposedDir = sign(Double(result.y - proposedContentOffset.y))
                if (proposedDir * decelerationDir == 1) || (proposedDir * decelerationDir == 0) { //same direction
                    if abs(result.y - proposedContentOffset.y) < 1 {
                        cv.setContentOffset(result, animated: true)
                        return cv.contentOffset
                    }
                    return result
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        cv.setContentOffset(result, animated: true)
                    }
                    return CGPoint(x: (cv.contentOffset.x + proposedContentOffset.x) / 2, y: (cv.contentOffset.y + proposedContentOffset.y) / 2)
                }
                
            } else {
                collectionView?.decelerationRate = .normal
            }
            return proposedContentOffset
        }
        
    }
}
