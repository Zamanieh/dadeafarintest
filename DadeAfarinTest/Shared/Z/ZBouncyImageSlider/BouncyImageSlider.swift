//
//  ClassHeaderView.swift
//  ZCodeBase
//
//  Created by Mr.Zee on 2/2/20.
//  Copyright © 2020 com.dotzee. All rights reserved.
//

import UIKit

class ZBouncyImageSlider: UIView {
    
    struct Image {
        var name: String? = nil
        var url: String? = nil
    }

    private var scrollViewHolder: UIView!
    private var pagedSV: UIScrollView!
    private var contentView: UIView!
    private var st: UIStackView!
    
    private var scrollViewTopCon: NSLayoutConstraint!
    private var ds: [Image] = []
    
    private var aspectRatio: CGFloat
    
    init(aspectRatio: CGFloat = 1.0) {
        self.aspectRatio = aspectRatio
        super.init(frame: .zero)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - FUNCTIONS
    private func initialize() {
        self.backgroundColor = .clear
        
        scrollViewHolder = UIView()
        scrollViewHolder.backgroundColor = .clear
        self.addSubview(scrollViewHolder)
        scrollViewHolder.constrain(to: self).leadingTrailingTopBottom()
        scrollViewHolder.constrainSelf().aspectRatio(self.aspectRatio)
        
        pagedSV = UIScrollView()
        pagedSV.showsHorizontalScrollIndicator = false
        pagedSV.isPagingEnabled = true
        self.scrollViewHolder.addSubview(pagedSV)
        pagedSV.constrain(to: self.scrollViewHolder).leadingTrailing().bottom()
        scrollViewTopCon = pagedSV.constrain(to: self.scrollViewHolder).top().constraints.first
        scrollViewTopCon.isActive = true
        pagedSV.constrainSelf().aspectRatio(self.aspectRatio, priority: .defaultHigh)
        
        contentView = UIView()
        contentView.backgroundColor = .clear
        self.pagedSV.addSubview(contentView)
        contentView.constrain(to: self.pagedSV).leadingTrailingTopBottom().height()
        
        st = UIStackView()
        st.alignment = .fill
        st.axis = .horizontal
        st.distribution = .fillEqually
        self.contentView.addSubview(st)
        st.constrain(to: self.contentView).leadingTrailingTopBottom()
    }

    
    private func reloadImages() {
        for view in self.st.arrangedSubviews {
            if let v = view as? ZBouncyImageView {
                v.removeFromSuperview()
            }
        }
        for item in ds {
            let item = ZBouncyImageView(image: item, contentMode: .scaleAspectFit)
            self.st.addArrangedSubview(item)
            item.constrain(to: self).width()
        }
    }
    
    // MARK: - PUBLIC FUNCTIONS
    public func set(model: [ZBouncyImageSlider.Image]) {
        self.ds = model
        reloadImages()
    }
    
    public func setOverOffset(_ overOffset: CGFloat) {
        self.scrollViewTopCon.constant = (overOffset)
    }
    
    public func bind(to pageControl: ZPageControl) {
        pageControl.bindScrollView(scrollView: self.pagedSV, delegate: nil)
    }
    
    
}
