//
//  Z.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 8/11/21.
//

import Foundation


class Z: NSObject {
    
    
    public static let math = ZMath()
    public static let ui = ZUI()
    public static let utils = ZUtils()
    
    
}
