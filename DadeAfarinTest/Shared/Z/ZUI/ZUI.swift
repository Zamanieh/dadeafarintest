//
//  ZUI.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 8/11/21.
//

import Foundation
import UIKit


class ZUI {
    
    
    open func `switch`(window toVC: UIViewController, completionHandler: ((Bool) -> Void)? = nil) {
        guard let window = UIApplication.shared.windows.filter(\.isKeyWindow).first else { return }
        window.rootViewController = toVC
        window.makeKeyAndVisible()
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion: { completed in
            completionHandler?(completed)
        })
    }
    
    open var topController: UIViewController? {
        get { return UIApplication.shared.windows.filter(\.isKeyWindow).first?.rootViewController }
    }
    
}
