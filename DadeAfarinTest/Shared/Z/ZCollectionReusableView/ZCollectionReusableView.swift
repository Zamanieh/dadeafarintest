//
//  ZCollectionReusableView.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import UIKit

fileprivate var reuseIdentifierKey = "reuseIdentifier"

class ZCollectionReusableView: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - FUNCTIONS
    open func initialize() {
        
    }
    
    public static func reuseIdentifier() -> String {
        var id = self.init().meta(for: reuseIdentifierKey) as? String
        if id == nil {
            id = NSStringFromClass(Self.self)
            self.init().set(meta: id!, for: reuseIdentifierKey)
        }
        return id!
    }
}
    

