//
//  PageControlScrollViewDelegateTrampoline.swift
//  Servus
//
//  Created by Mr Zee on 11/23/20.
//

import Foundation
import UIKit

class ZPageControlScrollViewDelegateTrampoline: ZTrampolin<UIScrollViewDelegate>, UIScrollViewDelegate {
    
    public var didScrollBlock: (UIScrollView, CGPoint) -> Void
    public var endOfScrollBlock: (UIScrollView) -> Void
    
    
    init(delegate: UIScrollViewDelegate?, didScrollBlock: @escaping (UIScrollView, CGPoint) -> Void, endOfScrollBlock: @escaping (UIScrollView) -> Void) {
        self.didScrollBlock = didScrollBlock
        self.endOfScrollBlock = endOfScrollBlock
        super.init(delegate: delegate)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.delegate?.responds(to: #selector(scrollViewDidScroll(_:))) ?? false {
            self.delegate?.scrollViewDidScroll?(scrollView)
        }
        self.didScrollBlock(scrollView, scrollView.contentOffset)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if self.delegate?.responds(to: #selector(scrollViewDidEndDragging(_:willDecelerate:))) ?? false {
            self.delegate?.scrollViewDidEndDragging?(scrollView, willDecelerate: decelerate)
        }
        decelerate ? () : (self.endOfScroll(scrollView: scrollView))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.delegate?.responds(to: #selector(scrollViewDidEndDecelerating(_:))) ?? false {
            self.delegate?.scrollViewDidEndDecelerating?(scrollView)
        }
        self.endOfScroll(scrollView: scrollView)
    }
    
    private func endOfScroll(scrollView: UIScrollView) {
        self.endOfScrollBlock(scrollView)
    }

    
}
