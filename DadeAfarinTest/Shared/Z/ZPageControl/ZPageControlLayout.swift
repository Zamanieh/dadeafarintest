//
//  PageControlLayout.swift
//  Servus
//
//  Created by Mr Zee on 11/22/20.
//

import UIKit

protocol ZPageControlLayoutDelegate: NSObject {
    
    func defaulSize(_ layout: ZPageControlLayout) -> CGFloat
    func selectedSize(_ layout: ZPageControlLayout) -> CGFloat
    func layout(_ layout: ZPageControlLayout, for cv: UICollectionView, didSet contentSize: CGSize)
    func selectedIndex(_ layout: ZPageControlLayout) -> CGFloat
}

class ZPageControlLayout: UICollectionViewLayout {

    
    private var cache: [ZStickyHeader.CollectionView.LayoutAttributes] = []
    private var defaultSize: CGFloat = 10
    private var selectedSize: CGFloat = 20
    private var progress: CGFloat = 0
    private var currentIndex: Int = 0
    
    public weak var delegate: ZPageControlLayoutDelegate?
    public var disableAnimation: Bool = false
    
    override func prepare() {
        super.prepare()
        cache = []
        guard let cv = collectionView else { return }
        let ds = delegate?.defaulSize(self) ?? defaultSize
        let ss = delegate?.selectedSize(self) ?? selectedSize
        let h = cv.frame.size.height - 2
        let count: CGFloat = CGFloat(cv.numberOfItems(inSection: 0))
        let interItemSpacing: CGFloat = 4
        let selectedIndex = delegate?.selectedIndex(self) ?? 0
        let leftIndex = floor(selectedIndex)
        let rightIndex = leftIndex + 1
        progress = selectedIndex - leftIndex
        
        let width1 = Z.math.lMapValue(value: 1-progress, srcLow: 0, srcHigh: 1, dstLow: ds, dstHigh: ss)
        let width2 = Z.math.lMapValue(value: progress, srcLow: 0, srcHigh: 1, dstLow: ds, dstHigh: ss)
        var xOffset: CGFloat = 0
        
        for i in 0..<Int(count) {
            let ip = IndexPath.init(item: i, section: 0)
            let attr = ZStickyHeader.CollectionView.LayoutAttributes.init(forCellWith: ip)
            var width: CGFloat = 0
            if i == Int(leftIndex) {
                attr.progress = 1-progress
                width = width1
            } else if i == Int(rightIndex) {
                attr.progress = progress
                width = width2
            } else {
                attr.progress = 0
                width = ds
            }
            attr.frame = CGRect.init(origin: CGPoint.init(x: xOffset, y: 1), size: CGSize.init(width: width, height: h))
            self.cache.append(attr)
            xOffset += interItemSpacing + width
        }
        cv.contentSize = .init(width: xOffset, height: h)
        delegate?.layout(self, for: cv, didSet: cv.contentSize)
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cache[indexPath.row]
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return self.cache
    }
    
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    // MARK: - FUNCTION
    private func getFuzzyIndex(_ x: CGFloat) -> CGFloat{
        return (x / (self.delegate?.defaulSize(self) ?? defaultSize))
    }
    
    private func getPrevIndex() -> Int{
        
        return Int(floor(getFuzzyIndex(self.collectionView?.contentOffset.x ?? 0)))
    }
    
    private func getNextIndex() -> Int{
        return Int(ceil(getFuzzyIndex(self.collectionView?.contentOffset.x ?? 0)))
    }

}
