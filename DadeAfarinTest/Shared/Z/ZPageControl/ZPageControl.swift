//
//  PageControl.swift
//  Servus
//
//  Created by Mr Zee on 11/22/20.
//

import UIKit

class ZPageControl: UIView, UICollectionViewDelegate, UICollectionViewDataSource, ZPageControlLayoutDelegate {

    private var cv: UICollectionView!
    
    private var layout: ZPageControlLayout!
    private var width: NSLayoutConstraint!
    private var count: Int = 0
    private var preciseSelectedIndex: CGFloat = 0
    private weak var boundScrollView: UIScrollView?
    private var proxyDelegate: ZPageControlScrollViewDelegateTrampoline?
    private var ignoreScrollViewDelegateCalls: Bool = false
    
    init(count: Int = 0) {
        self.count = count
        super.init(frame: .zero)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - FUNCTIONS
    private func initialize() {
        self.backgroundColor = .clear
        
        layout = ZPageControlLayout()
        layout.delegate = self
        
        cv = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        cv.delegate = self
        cv.dataSource = self
        cv.register(ZPageControlCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = .clear
        self.addSubview(cv)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        cv.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        cv.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        cv.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        width = cv.widthAnchor.constraint(equalToConstant: cv.contentSize.width)
        width.priority = .defaultHigh
        width.isActive = true
     }
    
    public func set(count: Int) {
        self.count = count
        self.cv.reloadData()
    }
    
    public func bindScrollView(scrollView: UIScrollView, delegate: UIScrollViewDelegate?) {
        self.boundScrollView = scrollView
        self.proxyDelegate = ZPageControlScrollViewDelegateTrampoline.init(delegate: delegate, didScrollBlock: {[weak self] scrollView, offset in
            guard let this = self else { return }
            if !this.ignoreScrollViewDelegateCalls {
                let p = offset.x / scrollView.bounds.size.width
                this.preciseSelectedIndex = max(CGFloat(min(0, max(this.count - 1, 0))), p)
                this.forceCVToLayout(animated: false)
            }
        }, endOfScrollBlock: {[weak self] scrollView in
            guard let this = self else { return }
            let p = scrollView.contentOffset.x / scrollView.bounds.size.width
            this.setSelected(index: Int(p), animated: true)
        })
        scrollView.delegate = self.proxyDelegate
    }
    
    private func forceCVToLayout(animated: Bool = true) {
        if animated {
            self.layout.disableAnimation = false
            self.cv.performBatchUpdates(nil, completion: {[weak self] finished in
                guard let this = self else { return }
                this.layout.disableAnimation = true
                this.layoutNoAnimation()
            })
        } else {
            self.layout.disableAnimation = true
            self.layoutNoAnimation()
        }
    }
    
    private func setSelected(index: Int, animated: Bool) {
        let indexPath = IndexPath.init(item: index, section: 0)
        self.preciseSelectedIndex = CGFloat(indexPath.item)
        self.forceCVToLayout(animated: animated)
        UIView.animate(withDuration: animated ? 0.3 : 0, animations: {
            self.ignoreScrollViewDelegateCalls = true
            self.boundScrollView?.setContentOffset(.init(x: self.preciseSelectedIndex * (self.boundScrollView?.bounds.size.width ?? 1), y: 0), animated: false)
        }, completion: { _ in
            self.ignoreScrollViewDelegateCalls = false
        })
    }
    
    private func layoutNoAnimation() {
        UIView.performWithoutAnimation {
            self.cv.performBatchUpdates(nil, completion: nil)
        }
    }
    
    
    
    // MARK: - UICOLLECTIONVIEW DELEGATE, DATASOURCE
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ZPageControlCell
        return cell
    }
    
    // MARK: - LAYOUT DELEGATE
    func defaulSize(_ layout: ZPageControlLayout) -> CGFloat {
        return 8
    }
    
    func selectedSize(_ layout: ZPageControlLayout) -> CGFloat {
        return 16
    }
    
    func layout(_ layout: ZPageControlLayout, for cv: UICollectionView, didSet contentSize: CGSize) {
        width.constant = cv.contentSize.width
        self.layoutIfNeeded()
    }
    
    func selectedIndex(_ layout: ZPageControlLayout) -> CGFloat {
        return self.preciseSelectedIndex
    }

}
