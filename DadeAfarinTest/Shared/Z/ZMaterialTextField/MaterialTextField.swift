//
//  MaterialTextField.swift
//  Lilas
//
//  Created by Mr.Zee on 4/1/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

protocol MaterialTextFieldDelegate: UITextFieldDelegate {
    func textField(_ textField: UITextField, didChange text: String)
}

enum SelectionPlaceHolderType { case hide, top}

struct MaterialTextFieldOptions {
    
    public var placeHolder: String
    public var tintColor: UIColor
    public var selectedColor: UIColor
    public var font: UIFont
    public var selectionPlaceHolder: SelectionPlaceHolderType
    public var icon: String?
    public var selectedIcon: String?
    public var borderHeight: CGFloat
    public var returnKey: UIReturnKeyType
    public var keyboardType: UIKeyboardType
    public var autocapitalizationType: UITextAutocapitalizationType
    public var padding: CGFloat
    public var ph_padding: CGFloat
    public var has_counter: Bool
    public var char_limit: Int
//    public var isDashedLine: Bool
    public var has_error: Bool
    public var border_color: UIColor?
    
    init(placeHolder: String,
         tintColor: UIColor = .white,
         selectedColor: UIColor = .white,
         font: UIFont,
         selectionPlaceHolderType: SelectionPlaceHolderType = .top,
         icon: String? = nil,
         selectedIcon: String? = nil,
         borderHeight: CGFloat = 1,
         returnKey: UIReturnKeyType = .default,
         keyboardType: UIKeyboardType = .default,
         autocapitalizationType: UITextAutocapitalizationType = .none,
         padding: CGFloat = 0,
         ph_padding: CGFloat = 0,
         has_counter: Bool = false,
         char_limit: Int = 0,
         isDashedLine: Bool = false,
         has_error: Bool = false,
         border_color: UIColor? = nil) {
        self.placeHolder = placeHolder
        self.tintColor = tintColor
        self.selectedColor = selectedColor
        self.selectionPlaceHolder = selectionPlaceHolderType
        self.icon = icon
        self.selectedIcon = selectedIcon
        self.borderHeight = borderHeight
        self.font = font
        self.returnKey = returnKey
        self.keyboardType = keyboardType
        self.autocapitalizationType = autocapitalizationType
        self.padding = padding
        self.ph_padding = ph_padding
        self.has_counter = has_counter
        self.char_limit = char_limit
//        self.isDashedLine = isDashedLine
        self.has_error = has_error
        self.border_color = border_color
    }
    
}

class MaterialTextField: UIView, UITextFieldDelegate {
    
    public var textField: UITextField!
    private var placeHolderLabel: UILabel!
    private var icon: UIImageView!
    private var iconBtn: UIButton!
    private var bottomBorder: UIView!
    private var errorLabel: UILabel!
    private var charCounterLabel: UILabel!
    private var btn: UIButton!
    
    private var options: MaterialTextFieldOptions
    private var placeHolderTop: NSLayoutConstraint!
    private var textFieldTop: NSLayoutConstraint!
    private var placeHolderBottom: NSLayoutConstraint!
    
    public var text: String = ""
    public var iconTapped: ((UITextField) -> Void)?
    public var keyboardReturnKeyTapped: ((UITextField, String) -> Bool)?
    public var isSecureTextEntry = false {
        didSet {
            self.textField.isSecureTextEntry = isSecureTextEntry
        }
    }
    public var errorMessage: String = "" {
        didSet{
            errorLabel.text = errorMessage
        }
    }
    public weak var delegate: MaterialTextFieldDelegate?
    
    override var canBecomeFocused: Bool {
        return true
    }

    init(_ options: MaterialTextFieldOptions) {
        self.options = options
        super.init(frame: .zero)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        if options.isDashedLine {
//            bottomBorder.createDashedLine(from: .init(x: 0, y: 0), to: .init(x: bottomBorder.frame.maxX, y: 0), width: 1)
//        }
    }
    
    // MARK: - FUNCTIONS
    private func initialize() {
        self.backgroundColor = .clear
        
        textField = UITextField()
        textField.textColor = options.selectedColor
        textField.font = options.font
        textField.delegate = self
        textField.borderStyle = .none
        textField.placeholder = nil
        textField.backgroundColor = .clear
        textField.returnKeyType = options.returnKey
        textField.keyboardType = options.keyboardType
        textField.autocapitalizationType = options.autocapitalizationType
        textField.isSecureTextEntry = self.isSecureTextEntry
        textField.addTarget(self, action: #selector(textFieldChanged(_ :)), for: .editingChanged)
        self.addSubview(textField)
        textField.constrain(to: self).leading()
        textFieldTop = textField.constrain(to: self).top(constant: getPlaceHolderSize()).constraints.first
        textField.constrainSelf().height(constant: 34)
        
        icon = UIImageView()
        if let i = options.icon {
            icon.image = UIImage(named: i)
        }
        icon.tintColor = options.tintColor
        self.addSubview(icon)
        if let _ = options.icon {
            icon.constrainSelf().aspectRatio(1)
        } else {
            icon.constrainSelf().width(constant: 0)
        }
        icon.constrainSelf().height(constant: 15)
        icon.constrain(to: self).trailing()
        icon.constrain(to: self.textField).centerY()
        
        iconBtn = UIButton()
        iconBtn.setTitle(nil, for: .normal)
        iconBtn.setImage(nil, for: .normal)
        iconBtn.addTarget(self, action: #selector(iconBtnTapped(_:)), for: .touchUpInside)
        self.addSubview(iconBtn)
        iconBtn.constrainSelf().widthHeight(constant: 44)
        iconBtn.constrain(to: self.icon).centerXY()
        
        charCounterLabel = UILabel()
       // charCounterLabel.font = UIFont.appFont.test.value(of: 10)
        charCounterLabel.textColor = options.tintColor
        charCounterLabel.text = "00/\(options.char_limit) Chars"
        charCounterLabel.textAlignment = .center
        self.addSubview(charCounterLabel)
        charCounterLabel.constrain(to: self.icon).xAxis(.trailing, to: .leading, constant: -self.options.padding)
        charCounterLabel.constrainSelf().width(constant: self.options.has_counter ? 70 : 0)
        charCounterLabel.constrain(to: self.textField).height().top()
        
        textField.constrain(to: self.charCounterLabel).xAxis(.trailing, to: .leading, constant: -5)
        
        bottomBorder = UIView()
        bottomBorder.backgroundColor = options.tintColor
        if let c = self.options.border_color {
            bottomBorder.backgroundColor = c
        }
        self.addSubview(bottomBorder)
        bottomBorder.constrain(to: self.textField).yAxis(.top, to: .bottom, constant: self.options.padding)
        bottomBorder.constrainSelf().height(constant: options.borderHeight)
        bottomBorder.constrain(to: self).leadingTrailing()
        
//        if options.isDashedLine {
            bottomBorder.backgroundColor = .clear
//        }
        
        errorLabel = UILabel()
        errorLabel.font = options.font.withSize(options.font.pointSize - 4)
        errorLabel.textColor = .red
//        errorLabel.textAlignment = Utilz.appIsRTL() ? .right : .left
        errorLabel.text = "Error"
        errorLabel.alpha = 0
        
        if options.has_error {
            self.addSubview(errorLabel)
            errorLabel.constrain(to: self).leading()
            errorLabel.constrain(to: self.bottomBorder).yAxis(.top, to: .bottom, constant: 3)
            errorLabel.constrain(to: self).bottom(priority: .defaultHigh)
        } else {
            bottomBorder.constrain(to: self).bottom()
        }
        
        placeHolderLabel = UILabel()
        placeHolderLabel.textColor = options.tintColor
        placeHolderLabel.font = options.font
        placeHolderLabel.text = options.placeHolder
        self.addSubview(placeHolderLabel)
        placeHolderLabel.constrain(to: self).leading()
        placeHolderTop = placeHolderLabel.constrain(to: self).top(constant: getPlaceHolderSize()).constraints.first
        placeHolderBottom = placeHolderLabel.constrain(to: self.textField).bottom(constant: options.ph_padding).constraints.first
        
        btn = UIButton()
        btn.setTitle(nil, for: .normal)
        btn.setImage(nil, for: .normal)
        btn.isHidden = true
        self.addSubview(btn)
        btn.constrain(to: self).leadingTrailingTopBottom()
    }
    
    @objc private func iconBtnTapped(_ sender: UIButton) {
        if let si = options.selectedIcon {
            self.icon.image = UIImage(named: si)
        }
        iconTapped?(self.textField)
    }
    
    private func editing(_ editing: Bool,_ animated: Bool = true) {
        switch options.selectionPlaceHolder {
        case .top:
            placeHolderLabel.textColor = editing ? options.selectedColor : options.tintColor
            if !self.text.isEmpty {
                break
            }
            placeHolderTop.constant = editing ? 0 : textFieldTop.constant
            placeHolderBottom.isActive = !editing
            
            UIView.animate(withDuration: animated ? 0.3 : 0, animations: {
                self.placeHolderLabel.transform = editing ? self.placeHolderLabel.transform.scaledBy(x: 0.8, y: 0.8).translatedBy(x: -0.2*self.placeHolderLabel.frame.size.width*0.8, y: 0) : .identity
                self.layoutIfNeeded()
            })
        case .hide:
            UIView.animate(withDuration: 0.3, animations: {
                self.placeHolderLabel.alpha = (editing || (!self.text.isEmpty)) ? 0 : 1
            })
        }
        if let c = options.border_color {
            bottomBorder.backgroundColor = c
        } else {
            bottomBorder.backgroundColor = editing ? options.selectedColor : options.tintColor
        }
        icon.tintColor = editing ? options.selectedColor : options.tintColor
        charCounterLabel.textColor = editing ? options.selectedColor : options.tintColor
    }
    
    private func getPlaceHolderSize() -> CGFloat {
        return (options.placeHolder.sizeOfString(usingFont: options.font).height * 0.8) + self.options.padding
    }
    
    private func setCharCount() {
        self.charCounterLabel.text = "\(String(format: "%02d", self.text.count))/\(self.options.char_limit) Chars"
    }
    
    private func changeText(_ string: String) {
        self.text = string
        setCharCount()
    }
    
    @objc private func textFieldChanged(_ sender: UITextField) {
        self.changeText(sender.text ?? "")
        delegate?.textField(self.textField, didChange: self.text)
    }
    
    public func setText(_ text: String) {
        self.editing(true, false)
        self.text = text
        self.textField.text = self.text
        self.editing(false, false)
    }
    
    public func set(_ placeHolder: String) {
        self.options.placeHolder = placeHolder
        self.placeHolderLabel.text = placeHolder
    }
    
    public func setInputView(_ picker: UIView) {
        self.textField.inputView = picker
    }
    
    public func addTarget(_ target: Any?, action: Selector, for event: UIControl.Event) {
        self.btn.isHidden = false
        self.btn.addTarget(target, action: action, for: event)
    }
    
    public func setSelectedIcon() {
        self.icon.image = UIImage(named: options.selectedIcon ?? "")
    }
    
    public func setDefaultIcon() {
        if let i = options.icon {
            self.icon.image = UIImage(named: i)
        }
    }
    
    public func showError(animated: Bool = true) {
        UIView.animate(withDuration: animated ? 0.3 : 0, animations: {
            self.errorLabel.alpha = 1
        })
    }
    
    public func hideError(animated: Bool = true) {
        UIView.animate(withDuration: animated ? 0.3 : 0, animations: {
            self.errorLabel.alpha = 0
        })
    }
    
    @discardableResult override func becomeFirstResponder() -> Bool {
        textField.becomeFirstResponder()
    }
    
    @discardableResult override func resignFirstResponder() -> Bool {
        textField.resignFirstResponder()
    }
    
    public var nextFieldToFocus: (() -> UIView?)?
    
    // MARK: - TEXTFIELD DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.text = textField.text ?? ""
        if let fnc = keyboardReturnKeyTapped {
            return fnc(textField, self.text)
        }
        if let next = nextFieldToFocus, let view = next() {
            resignFirstResponder()
            view.becomeFirstResponder()
            return true
        }
        if options.returnKey == .done {
            resignFirstResponder()
        }
        return delegate?.textFieldShouldReturn?(textField) ?? true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.editing(true)
        return delegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.changeText(self.textField.text ?? "")
        self.editing(false)
        return delegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if options.has_counter{
            if self.text.count < options.char_limit {
                return delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
            } else {
                if string.isBackspace {
                    return delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
                } else {
                    return delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? false
                }
            }
        } else {
            self.changeText(string)
            return delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
        }
    }
    
}

