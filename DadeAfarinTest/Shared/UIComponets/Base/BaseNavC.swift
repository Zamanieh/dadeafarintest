//
//  BaseNavC.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 7/21/21.
//

import UIKit


/// empty backgourd navigation bar
class BaseNavC: UINavigationController {
    
    public var effect: ZBlurVisualEffectView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.statusBarStyle
    }
    
    public var statusBarStyle: UIStatusBarStyle = .default

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .app.pure_white.dynamic
        
        self.navigationItem.hidesBackButton = true
        self.navigationBar.isTranslucent = true
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.backgroundColor = nil
        self.navigationBar.barTintColor = nil
        self.navigationBar.tintColor = nil
        self.navigationBar.isOpaque = true
        effect = ZBlurVisualEffectView.init(radius: 0.5, style: .extraLight)
        effect.isUserInteractionEnabled = false
        effect.layer.zPosition = -10000
        self.navigationBar.addSubview(effect)
        effect.constrain(to: self.navigationBar).leadingTrailing().bottom().top(constant: -UIDevice.current.statusBarHeight)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        effect.frame = .init(x: 0, y: -UIDevice.current.statusBarHeight, width: UIScreen.main.bounds.size.width, height: UIDevice.current.statusBarHeight + UINavigationController.navBarHeight)
    }
    
    public func configureEffect(show: Bool) {
        UIView.animate(withDuration: 0.3, animations: {
            self.effect.alpha = show ? 1 : 0
        })
    }

}
