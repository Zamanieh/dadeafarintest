//
//  category.swift
//  DadeAfarinTest
//
//  Created by MohammadReza Zamanieh on 8/23/21.
//

import Foundation


extension Model {
    
    struct category: Codable {
        
        public var id: Int
        public var name: String
        public var image: String
        
        
    }
    
    
}
