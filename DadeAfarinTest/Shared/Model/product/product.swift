//
//  product.swift
//  DadeAfarinTest
//
//  Created by MohammadReza Zamanieh on 8/23/21.
//

import Foundation


extension Model {
    
    struct product: Codable {
        
        public var id: Int
        public var name: String
        public var image: String
        public var description: String
        public var category: Model.category
        
    }
    
}
