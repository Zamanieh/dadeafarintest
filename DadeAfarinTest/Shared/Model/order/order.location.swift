//
//  order.location.swift
//  DadeAfarinTest
//
//  Created by MohammadReza Zamanieh on 8/24/21.
//

import Foundation


extension Model.order {
    
    struct location: Codable {
        
        public var latitude: Double
        public var longitude: Double
        
    }
    
    
}
