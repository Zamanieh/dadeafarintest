//
//  order.swift
//  DadeAfarinTest
//
//  Created by MohammadReza Zamanieh on 8/23/21.
//

import Foundation


extension Model {
    
    struct order: Codable {
        
        enum status: Int {
            case pending = 0
            case process = 1
            case delivery = 2
            case delivered = 3
        }
        
        public var id: Int
        public var product: Model.product
        public var status: Int
        public var location: Model.order.location
        
    }
    
    
}
