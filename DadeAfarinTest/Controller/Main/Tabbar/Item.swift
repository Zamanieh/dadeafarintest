//
//  Item.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 7/11/21.
//

import UIKit

protocol TabbarItemDelegate: NSObject {
    
    func item(_ item: Main.Tabbar.Item, didSelectAt index: Int)
    
}

extension Main.Tabbar {

    class Item: UIView {
        
        private var imageView: UIImageView!
        private var titleLabel: UILabel!
        private var btn: UIButton!
        
        public var isSelected: Bool = false
        public weak var delegate: TabbarItemDelegate?
        
        private var index: Int
        private var title: String
        private var image: UIImage?
        
        init(image: UIImage?, title: String, index: Int) {
            self.image = image
            self.title = title
            self.index = index
            super.init(frame: .zero)
            self.initialize()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        // MARK: - FUNCTIONS
        
        private func initialize() {
            self.backgroundColor = .clear
            self.constrainSelf().aspectRatio(1)
            
            imageView = UIImageView()
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            imageView.tintColor = UIColor.app.pure_black.dynamic
            self.addSubview(imageView)
            imageView.constrain(to: self).top(constant: 8).leadingTrailing()
            
            titleLabel = UILabel()
            titleLabel.text = title
            titleLabel.textColor = UIColor.app.pure_black.dynamic
            titleLabel.textAlignment = .center
            titleLabel.font = UIFont.systemFont(ofSize: 10)
            titleLabel.adjustsFontSizeToFitWidth = true
            self.addSubview(titleLabel)
            titleLabel.constrain(to: self).leadingTrailing().bottom()
            titleLabel.constrain(to: self.imageView).yAxis(.top, to: .bottom, constant: 4)
            
            btn = UIButton()
            btn.setTitle(nil, for: .normal)
            btn.setImage(nil, for: .normal)
            btn.addTarget(self, action: #selector(btnTapped(_:)), for: .touchUpInside)
            self.addSubview(btn)
            btn.constrain(to: self).leadingTrailingTopBottom()
            
        }
        
        @objc private func btnTapped(_ sender: UIButton) {
            if isSelected { return }
            delegate?.item(self, didSelectAt: self.index)
        }
        
        public func set(selected: Bool, animated: Bool = true) {
            self.isSelected = selected
            UIView.animate(withDuration: animated ? 0.3 : 0, animations: {
                self.imageView.tintColor = UIColor.app.pure_black.dynamic.withAlphaComponent(self.isSelected ? 1 : 0.5)
                self.titleLabel.textColor = UIColor.app.pure_black.dynamic.withAlphaComponent(self.isSelected ? 1 : 0.5)
            })
        }
        
        public func indexReturn() -> Int {
            return self.index
        }
        
    }
    
}
