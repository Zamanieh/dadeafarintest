//
//  Tabbar.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 7/11/21.
//

import UIKit

protocol TabbarDelegate: NSObject {
    func tabbar(_ bar: Main.Tabbar, didSelectItemAt index: Int)
}

extension Main {

    class Tabbar: UIView, TabbarItemDelegate {
        
        private var bg: ZBlurVisualEffectView!
        private var container: UIView!
        private var st: UIStackView!
        
        public weak var delegate: TabbarDelegate?
        
        init() {
            super.init(frame: .zero)
            self.initialize()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.backgroundColor = .clear
            self.constrainSelf().height(constant: UITabBar.tabBarHeight + UIDevice.current.bottomNotch)
            
            bg = ZBlurVisualEffectView.init(radius: 0.5, style: .extraLight)
            self.addSubview(bg)
            bg.constrain(to: self).leadingTrailingTopBottom()
            
            container = UIView()
            container.backgroundColor = .clear
            self.bg.contentView.addSubview(container)
            container.constrain(to: self).leadingTrailing().top()
            container.constrainSelf().height(constant: UITabBar.tabBarHeight)
            
            st = UIStackView()
            st.alignment = .fill
            st.axis = .horizontal
            st.distribution = .fillProportionally
            st.spacing = 48
            self.container.addSubview(st)
            st.constrain(to: self.container).centerX().topBottom().leadingTrailing(.greaterThanOrEqual, constant: 24)
            
        }
        
        public func configure(with items: [Tabbar.Item]) {
            for view in self.st.arrangedSubviews {
                if let v = view as? Tabbar.Item {
                    v.removeFromSuperview()
                }
            }
            for (_,item) in items.enumerated() {
                item.delegate = self
                self.st.addArrangedSubview(item)
            }
        }
        
        private func deselectAll() {
            for view in self.st.arrangedSubviews {
                if let v = view as? Tabbar.Item {
                    v.set(selected: false)
                }
            }
        }
        
        private func selectItem(at index: Int) {
            self.deselectAll()
            for view in self.st.arrangedSubviews {
                if let v = view as? Tabbar.Item, v.indexReturn() == index {
                    v.set(selected: true)
                }
            }
            delegate?.tabbar(self, didSelectItemAt: index)
        }
        
        public func select(index: Int) {
            self.selectItem(at: index)
        }
        
        // MARK: - TABBAR ITEM DELEGATE
        func item(_ item: Item, didSelectAt index: Int) {
            self.selectItem(at: index)
        }
        
    }
    
}
