//
//  Main.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 7/11/21.
//

import Foundation
import UIKit

struct Main { }


extension Main {
    
    class vc: UIViewController, TabbarDelegate, UINavigationControllerDelegate {
        
        private var categoryNav: BaseNavC!
        private var category: Category.vc!
        
        private var orders: Order.list.vc!
        private var ordersNav: BaseNavC!
        
        private var container: UIView!
        private var tabbar: Tabbar!
        
        private var tabbarBottom: NSLayoutConstraint!
        private var lastShownVC: UIViewController?
        private var lastIndex: Int?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.view.frame = UIScreen.main.bounds
            self.view.backgroundColor = .clear
            
            category = Category.vc()
            categoryNav = BaseNavC.init(rootViewController: category)
            categoryNav.delegate = self
            
            orders = Order.list.vc()
            ordersNav = BaseNavC.init(rootViewController: orders)
            ordersNav.delegate = self
            
            container = UIView()
            container.backgroundColor = UIColor.app.pure_black.value
            self.view.addSubview(container)
            container.constrain(to: self.view).leadingTrailingTopBottom()
            
            tabbar = Tabbar()
            tabbar.delegate = self
            self.view.addSubview(tabbar)
            tabbar.constrain(to: self.view).leadingTrailing()
            tabbarBottom = tabbar.constrain(to: self.view).bottom().constraints.first
            tabbarBottom.isActive = true
            
            tabbar.configure(with: [.init(image: UIImage.app.category.value, title: "Categories", index: 0), .init(image: UIImage.app.order.value, title: "Orders", index: 1)])
            
            self.tabbar.select(index: 0)
            

        }
        
        /// position for tabbar and current music if shown
        private func positionBottom(for nav: UINavigationController) {
            let shouldHide = nav.viewControllers.count > 1
            let finalBottomInset = shouldHide ? -(UITabBar.tabBarHeight + UIDevice.current.bottomNotch) : 0
            if finalBottomInset == self.tabbarBottom.constant { return }
            self.tabbarBottom.constant = finalBottomInset
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        /// change current controller
        private func change(child vc: UIViewController) {
            if vc == lastShownVC { return }
            if let vc = lastShownVC {
                self.removeVC(vc)
            }
            self.add(child: vc)
        }
        
        /// add new controller
        private func add(child vc: UIViewController) {
            self.lastShownVC = vc
            self.addChildVC(containerView: self.container, vc)
        }
        
        /// remove controller
        private func removeVC(_ vc: UIViewController, completion: (() -> Void)? = nil) {
            guard self.container.subviews.count > 0 else { return }
            self.removeChildVC(vc, completion: completion)
        }
        
        /// change current tab
        public func go(to index: Int) {
            self.tabbar.select(index: index)
        }
        
        // MARK: - TABBAR DELEGATE
        func tabbar(_ bar: Main.Tabbar, didSelectItemAt index: Int) {
            if index == lastIndex { return }
            switch index {
            case 0:
                self.change(child: self.categoryNav)
                self.lastIndex = index
            case 1:
                self.change(child: self.ordersNav)
                self.lastIndex = index
            default:
                self.go(to: 0)
            }
        }
        
        // MARK: - UINAVIGATION CONTROLLER DELEGATE
        func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
            self.positionBottom(for: navigationController)
        }
        
        func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
            self.positionBottom(for: navigationController)
        }
        
    }
    
}
