//
//  Home.vc.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit
import RxSwift

fileprivate var minimumCellSize: CGFloat = 140.0
fileprivate var itemSpacing: CGFloat = 24.0

extension Category {
    
    class vc: ZBaseVC, UICollectionViewDelegate, UICollectionViewDataSource {
        
        private var layout: UICollectionViewFlowLayout!
        private var cv: UICollectionView!
        
        private var ds: [Model.category] = [] {
            didSet {
                self.cv.reloadData()
            }
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            let width = cv.itemWidth(interItemSpacing: itemSpacing, minCellWidth: minimumCellSize)
            layout.itemSize = .init(width: width, height: width + 30)
            self.cv.collectionViewLayout = layout
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.view.backgroundColor = UIColor.app.pure_white.dynamic
            self.title = "Categories"
            
            layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .vertical
            layout.minimumLineSpacing = itemSpacing
            layout.minimumInteritemSpacing = itemSpacing
            
            cv = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
            cv.backgroundColor = .clear
            cv.delegate = self
            cv.dataSource = self
            cv.register(Category.cell.self, forCellWithReuseIdentifier: Category.cell.reuseIdentifier())
            cv.contentInset = .init(top: 12, left: 12, bottom: UIDevice.current.bottomNotch + UITabBar.tabBarHeight, right: 12)
            self.view.addSubview(cv)
            cv.constrain(to: self.view).leadingTrailingTopBottom()
            
            self.ds = [DataSource.cat1, DataSource.cat2, DataSource.cat3, DataSource.cat4, DataSource.cat5, DataSource.cat6, DataSource.cat7, DataSource.cat8, DataSource.cat9]
            self.cv.reloadData()
        }
        
        // MARK: - CV DELEGATE, DATASOURCE
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.ds.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Category.cell.reuseIdentifier(), for: indexPath) as! Category.cell
            cell.configure(with: self.ds[indexPath.item])
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let vc = Product.list.vc.init(category: self.ds[indexPath.item])
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    
}
