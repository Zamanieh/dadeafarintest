//
//  Home.cell.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit


extension Category {
    
    class cell: ZCollectionViewCell {
        
        private var shadow: UIView!
        private var imageView: UIImageView!
        private var titleLabel: UILabel!
        
        private var ds: Model.category!
        
        override func initialize() {
            super.initialize()
            self._initialize()
        }
        
        // MARK: - FUNCTIONS
        private func _initialize() {
            self.clipsToBounds = false
            self.contentView.clipsToBounds = false
            
            shadow = UIView()
            shadow.backgroundColor = .app.pure_white.dynamic
            shadow.layer.cornerRadius = AppConstant.k.cornerRadius
            shadow.dropShadow(.app.pure_black.dynamic, opacity: 0.2, .zero, radius: 4)
            self.contentView.addSubview(shadow)
            shadow.constrain(to: self.contentView).leadingTrailing().top()
            shadow.constrainSelf().aspectRatio(1)
            
            imageView = UIImageView()
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.layer.cornerRadius = AppConstant.k.cornerRadius
            self.shadow.addSubview(imageView)
            imageView.constrain(to: self.shadow).leadingTrailingTopBottom()
            
            titleLabel = UILabel()
            titleLabel.textColor = .app.pure_black.dynamic
            titleLabel.font = .systemFont(ofSize: 14)
            titleLabel.textAlignment = .center
            titleLabel.numberOfLines = 0
            self.contentView.addSubview(titleLabel)
            titleLabel.constrain(to: self.contentView).leadingTrailing().bottom()
            titleLabel.constrain(to: self.shadow).yAxis(.top, to: .bottom, constant: 8)
            
        }
        
        public func configure(with category: Model.category) {
            self.ds = category
            imageView.image = UIImage(named: category.image)
            titleLabel.text = category.name
        }
    }
    
}
