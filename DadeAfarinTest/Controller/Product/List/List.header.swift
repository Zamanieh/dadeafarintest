//
//  List.header.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit

fileprivate var imageSize: CGFloat = 160
fileprivate var halfBtnSize: CGFloat = 30


extension Product.list {
    
    class header: ZCollectionReusableView {
        
        private var container: ZBlurVisualEffectView!
        private var imageView: UIImageView!
        private var titleLabel: UILabel!
        
        private var gradient: CAGradientLayer!
        
        private var titleTop: NSLayoutConstraint!
        private var imageConst: NSLayoutConstraint!
        
        
        override func initialize() {
            super.initialize()
            self._initialize()
        }
        
        override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
            super.apply(layoutAttributes)
            guard let attr = layoutAttributes as? ZStickyHeader.CollectionView.LayoutAttributes else { return }
            self.refresh(with: attr.collapseProgress)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            self.gradient.frame = self.container.contentView.bounds
        }
        
        // MARK: - FUNCTIONS
        private func _initialize() {
            self.clipsToBounds = false
            
            container = ZBlurVisualEffectView.init(radius: 0.5, style: .extraLight)
            container.contentView.backgroundColor = .clear
            self.addSubview(container)
            container.constrain(to: self).leadingTrailingTopBottom()
            
            gradient = CAGradientLayer()
            gradient.colors = []
            self.container.contentView.layer.addSublayer(gradient)
            
            imageView = UIImageView()
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.layer.cornerRadius = 8
            self.container.contentView.addSubview(imageView)
            imageView.constrain(to: self.container.contentView).centerX().leadingTrailing(.greaterThanOrEqual).top(constant: UINavigationController.navBarHeight + UIDevice.current.statusBarHeight + 24)
            imageView.constrainSelf().aspectRatio(1)
            imageConst = imageView.constrainSelf().width(constant: imageSize).constraints.first
            imageConst.isActive = true
            
            titleLabel = UILabel()
            titleLabel.textColor = UIColor.app.pure_black.dynamic
            titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
            titleLabel.textAlignment = .center
            titleLabel.numberOfLines = 0
            self.container.contentView.addSubview(titleLabel)
            titleLabel.constrain(to: self).leadingTrailing(constant: 12)
            titleTop = titleLabel.constrain(to: self.container.contentView).top(constant: imageSize + 48 + UINavigationController.navBarHeight + UIDevice.current.statusBarHeight).constraints.first // 24 y from image + 24 image from top
            titleTop.isActive = true
            

        }
        
        /// position view with collapse progress
        private func refresh(with progress: CGFloat) {
            let imageProg = Z.math.lMapValue(value: 1-progress, srcLow: 0, srcHigh: 1, dstLow: 0, dstHigh: imageSize)
            let titleTopMin = UIDevice.current.statusBarHeight + ((UINavigationController.navBarHeight / 2) - (self.titleLabel.frame.size.height / 2))
            let titleProg = Z.math.lMapValue(value: 1-progress, srcLow: 0, srcHigh: 1, dstLow: titleTopMin, dstHigh: imageSize + 48 + UINavigationController.navBarHeight + UIDevice.current.statusBarHeight)
            imageConst.constant = imageProg
            titleTop.constant = titleProg
            self.imageView.alpha = 1-progress
        }
        
        
        public func configure(with category: Model.category) {
            self.imageView.image = UIImage(named: category.image)
            self.titleLabel.text = category.name
            if let c = self.imageView.image?.averageColor {
                self.gradient.colors = [c.withAlphaComponent(0.8).cgColor, c.withAlphaComponent(0.6).cgColor, c.withAlphaComponent(0.4).cgColor, c.withAlphaComponent(0.2).cgColor, c.withAlphaComponent(0.02).cgColor]
            }
        }
        
    }
    
}
