//
//  List.vc.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit


extension Product.list {
    
    class vc: ZBaseVC, ZStickyHeaderCollectionViewFlowLayoutDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ProductListCellDelegate {
        
        private var more: UIBarButtonItem!
        private var fav: UIBarButtonItem!
        private var layout: ZStickyHeader.CollectionView.FlowLayout!
        private var cv: UICollectionView!
        
        private var category: Model.category
        private var ds: [Model.product] = [] {
            didSet {
                self.cv.reloadData()
            }
        }
        
        init(category: Model.category) {
            self.category = category
            super.init(nibName: nil, bundle: nil)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            (self.navigationController as? BaseNavC)?.configureEffect(show: false)
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            (self.navigationController as? BaseNavC)?.configureEffect(show: true)
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.view.backgroundColor = UIColor.app.pure_white.dynamic
            
            layout = ZStickyHeader.CollectionView.FlowLayout()
            layout.delegate = self
            
            cv = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
            cv.delegate = self
            cv.dataSource = self
            cv.register(Product.list.cell.self, forCellWithReuseIdentifier: Product.list.cell.reuseIdentifier())
            cv.register(Product.list.header.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Product.list.header.reuseIdentifier())
            cv.backgroundColor = .clear
            cv.showsVerticalScrollIndicator = false
            self.view.addSubview(cv)
            cv.constrain(to: self.view).leadingTrailing().bottom().top(constant: -UIDevice.current.statusBarHeight - UINavigationController.navBarHeight)
            
            
            ds = [AppConstant.ds.pro1, AppConstant.ds.pro2, AppConstant.ds.pro3, AppConstant.ds.pro4, AppConstant.ds.pro5, AppConstant.ds.pro6, AppConstant.ds.pro7, AppConstant.ds.pro8, AppConstant.ds.pro9]
        }
        
        private func goToPick(with product: Model.product) {
            let vc = Order.picker.vc.init(product: product)
            self.navigationController?.pushViewController(vc, animated: true)
        }

        
        // MARK: - CV DELEGATE, DATASOURCE
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.ds.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Product.list.cell.reuseIdentifier(), for: indexPath) as! Product.list.cell
            cell.configure(with: self.ds[indexPath.item])
            cell.delegate = self
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            self.goToPick(with: self.ds[indexPath.item])
        }
        
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            if kind == UICollectionView.elementKindSectionHeader {
                let v = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Product.list.header.reuseIdentifier(), for: indexPath) as! Product.list.header
                v.configure(with: self.category)
                return v
            } else {
                fatalError("not configured element kind")
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return .init(width: self.view.frame.size.width, height: 110)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 12
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 12
        }
        
        // MARK: - STICKY HEADER DELEGATE
        func headerSize(layout: ZStickyHeader.CollectionView.FlowLayout, collectionViewSize: CGSize) -> ZStickyHeader.CollectionView.HeaderSize {
            let defaultHeight: CGFloat = UINavigationController.navBarHeight + UIDevice.current.topNotch
            return .init(minimumHeight: defaultHeight, normalHeight: defaultHeight + 330)
        }
        
        // MARK: - CELL DELEGATE
        func cell(_ cell: Product.list.cell, buy model: Model.product) {
            self.goToPick(with: model)
        }

        
    }
    
    
}
