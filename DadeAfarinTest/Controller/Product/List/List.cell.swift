//
//  List.cell.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit


protocol ProductListCellDelegate: NSObject {
    
    func cell(_ cell: Product.list.cell, buy model: Model.product)
    
}

extension Product.list {
    
    class cell: ZCollectionViewCell {
        
        private var card: UIView!
        private var imageView: UIImageView!
        private var titleLabel: UILabel!
        private var descLabel: UILabel!
        private var buyBtn: UIButton!
        
        private var ds: Model.product!
        
        public weak var delegate: ProductListCellDelegate?
        
        override func initialize() {
            super.initialize()
            self._initialize()
        }
        
        // MARK: - FUNCTIONS
        private func _initialize() {
            
            card = UIView()
            card.backgroundColor = .app.pure_white.dynamic
            card.layer.cornerRadius = 12
            card.dropShadow(.app.pure_black.dynamic, opacity: 0.2, .init(width: 0, height: 2), radius: 4)
            self.contentView.addSubview(card)
            card.constrain(to: self.contentView).leadingTrailing(constant: 12).top(constant: 8).bottom()
            
            imageView = UIImageView()
            imageView.layer.cornerRadius = 8
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            self.card.addSubview(imageView)
            imageView.constrain(to: self.card).leading(constant: 8).topBottom(constant: 8)
            imageView.constrainSelf().aspectRatio(1)
            
            titleLabel = UILabel()
            titleLabel.textColor = UIColor.app.pure_black.dynamic
            titleLabel.font = UIFont.systemFont(ofSize: 14)
            titleLabel.textAlignment = .left
            titleLabel.numberOfLines = 0
            self.card.addSubview(titleLabel)
            titleLabel.constrain(to: self.card).top(constant: 12)
            titleLabel.constrain(to: self.imageView).xAxis(.leading, to: .trailing, constant: 16)
            
            descLabel = UILabel()
            descLabel.textColor = UIColor.app.gray.dynamic
            descLabel.font = UIFont.systemFont(ofSize: 12)
            descLabel.textAlignment = .left
            descLabel.numberOfLines = 0
            self.card.addSubview(descLabel)
            descLabel.constrain(to: self.titleLabel).leadingTrailing().yAxis(.top, to: .bottom, constant: 4)
            descLabel.constrain(to: self.card).bottom(constant: 12)
            
            buyBtn = UIButton()
            buyBtn.setTitle("BUY", for: .normal)
            buyBtn.setImage(nil, for: .normal)
            buyBtn.setTitleColor(.app.pure_white.value, for: .normal)
            buyBtn.titleLabel?.font  = .boldSystemFont(ofSize: 16)
            buyBtn.backgroundColor = .app.green.value
            buyBtn.layer.cornerRadius = 20
            buyBtn.dropShadow(.app.green.value, opacity: 0.3, .zero, radius: 4)
            buyBtn.addTarget(self, action: #selector(buyBtnTapped(_:)), for: .touchUpInside)
            self.card.addSubview(buyBtn)
            buyBtn.constrain(to: self.card).trailing(constant: 8).centerY()
            buyBtn.constrainSelf().height(constant: 40).width(constant: 80)
            buyBtn.constrain(to: self.titleLabel).xAxis(.leading, to: .trailing, constant: 16)
            
        }
        
        @objc private func buyBtnTapped(_ sender: UIButton) {
            delegate?.cell(self, buy: self.ds)
        }
        
        public func configure(with model: Model.product) {
            self.ds = model
            self.imageView.image = UIImage(named: model.image)
            self.titleLabel.text = model.name
            self.descLabel.text = model.description
        }
        
    }
    
}
