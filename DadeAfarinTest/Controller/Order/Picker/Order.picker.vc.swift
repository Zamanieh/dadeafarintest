//
//  Order.picker.vc.swift
//  DadeAfarinTest
//
//  Created by MohammadReza Zamanieh on 8/24/21.
//

import Foundation
import UIKit
import MapKit


extension Order.picker {
    
    class vc: ZBaseVC, CLLocationManagerDelegate {
        
        private var map: MKMapView!
        private var location: CLLocationManager!
        private var mark: UIImageView!
        private var chooseBtn: UIButton!
        
        private var product: Model.product
        
        init(product: Model.product) {
            self.product = product
            super.init(nibName: nil, bundle: nil)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.title = "Order \(self.product.name)"
            
            location = CLLocationManager()
            location.delegate = self
            
            map = MKMapView()
            self.view.addSubview(map)
            map.constrain(to: self.view).leadingTrailingTopBottom()
            
            mark = UIImageView()
            mark.contentMode = .scaleAspectFit
            mark.image = .app.mark.value
            mark.tintColor = .app.red.value
            self.view.addSubview(mark)
            mark.constrain(to: self.view).centerXY()
            mark.constrainSelf().width(constant: 36).aspectRatio(1)
            
            chooseBtn = UIButton()
            chooseBtn.backgroundColor = .app.green.value
            chooseBtn.setTitle("Confirm", for: .normal)
            chooseBtn.setTitleColor(.app.pure_white.value, for: .normal)
            chooseBtn.titleLabel?.font = .boldSystemFont(ofSize: 16)
            chooseBtn.layer.cornerRadius = 8
            chooseBtn.dropShadow(.app.green.value, opacity: 0.3, .init(width: 0, height: 3), radius: 6)
            chooseBtn.addTarget(self, action: #selector(chooseBtnTapped(_:)), for: .touchUpInside)
            self.view.addSubview(chooseBtn)
            chooseBtn.constrain(to: self.view).leadingTrailing(constant: 16).bottom(constant: UIDevice.current.bottomNotch + 20)
            chooseBtn.constrainSelf().height(constant: 50)
            
            
        }
        
        private func centerToLocation(_ location: CLLocation, regionRadius: CLLocationDistance = 1000) {
            let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
            self.map.setRegion(coordinateRegion, animated: true)
        }
        
        @objc private func chooseBtnTapped(_ sender: UIButton) {
            let coord = self.map.convert(self.map.center, toCoordinateFrom: self.view)
            let order = Model.order.init(id: AppConstant.cart.newId(), product: self.product, status: 0, location: .init(latitude: coord.latitude, longitude: coord.longitude))
            AppConstant.cart.add(new: order)
            self.navigationController?.popToRootViewController(animated: true)
            (Z.ui.topController as? Main.vc)?.go(to: 1)
        }
        
        // MARK: - LOCATION DELEGATE
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            if let location = locations.last {
                self.centerToLocation(location)
            }
            manager.stopUpdatingLocation()
        }
        
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            switch status {
            case .notDetermined:
                manager.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                manager.requestLocation()
            default:
                break
            }
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            // show error
        }
        
        
    }
    
}
