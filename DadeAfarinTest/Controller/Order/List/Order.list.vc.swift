//
//  Order.list.vc.swift
//  DadeAfarinTest
//
//  Created by MohammadReza Zamanieh on 8/24/21.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa


extension Order.list {
    
    class vc: ZBaseVC, UITableViewDelegate, UITableViewDataSource {
        
        
        private var table: UITableView!
        
        private var ds: [Model.order] = AppConstant.cart.retrieve() ?? [] {
            didSet {
                DispatchQueue.main.async {
                    self.table.reloadData()
                    self.table.layoutIfNeeded()
                }
            }
        }
        private var disposeBag = DisposeBag()
        private var timer: Timer!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.ds = AppConstant.cart.retrieve() ?? []
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.title = "Orders"
            self.view.backgroundColor = .app.pure_white.dynamic
            
            table = UITableView()
            table.backgroundColor = .clear
            table.delegate = self
            table.dataSource = self
            table.register(Order.list.cell.self, forCellReuseIdentifier: Order.list.cell.reuseIdentifier())
            table.separatorStyle = .none
            table.contentInset.bottom = UINavigationController.navBarHeight + UIDevice.current.bottomNotch
            self.view.addSubview(table)
            table.constrain(to: self.view).leadingTrailingTopBottom()
            
            timer = Timer.init(timeInterval: 5, target: self, selector: #selector(timerFired(_:)), userInfo: nil, repeats: true)
            timer.tolerance = 1
            RunLoop.main.add(timer, forMode: .default)
            
        }
        
        @objc private func timerFired(_ sender: Timer) {
            self.ds = AppConstant.cart.retrieve() ?? []
        }
        
        // MARK: - TABLE DELEGATE, DATASOURCE
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return ds.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: Order.list.cell.reuseIdentifier()) as! Order.list.cell
            cell.configure(with: self.ds[indexPath.row])
            return cell
        }
        
    }
    
}
