//
//  Order.list.cell.swift
//  DadeAfarinTest
//
//  Created by MohammadReza Zamanieh on 8/24/21.
//

import Foundation
import UIKit


extension Order.list {
    
    
    class cell: ZTableViewCell {
        
        private var card: UIView!
        private var image: UIImageView!
        private var titleLabel: UILabel!
        private var locationLabel: UILabel!
        private var statusBg: UIView!
        private var statusLabel: UILabel!
        
        
        override func initialize() {
            super.initialize()
            self._initialize()
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            self.statusBg.layer.cornerRadius = self.statusBg.frame.size.height / 2
        }
        
        // MARK: - FUNCTIONS
        private func _initialize() {
            self.clipsToBounds = false
            self.contentView.clipsToBounds = false
            
            card = UIView()
            card.backgroundColor = .app.pure_white.dynamic
            card.layer.cornerRadius = 12
            card.dropShadow(.app.pure_black.dynamic, opacity: 0.2, .init(width: 0, height: 2), radius: 4)
            self.contentView.addSubview(card)
            card.constrain(to: self.contentView).leadingTrailing(constant: 12).top(constant: 12).bottom()
            
            image = UIImageView()
            image.layer.cornerRadius = 8
            image.contentMode = .scaleAspectFill
            image.clipsToBounds = true
            self.card.addSubview(image)
            image.constrain(to: self.card).leading(constant: 8).top(constant: 8).bottom(.greaterThanOrEqual, constant: 8)
            image.constrainSelf().aspectRatio(1).height(constant: 64)
            
            titleLabel = UILabel()
            titleLabel.textColor = .app.pure_black.dynamic
            titleLabel.font = .systemFont(ofSize: 16)
            titleLabel.numberOfLines = 0
            titleLabel.textAlignment = .left
            self.card.addSubview(titleLabel)
            titleLabel.constrain(to: self.card).top(constant: 12)
            titleLabel.constrain(to: self.image).xAxis(.leading, to: .trailing, constant: 16)
            
            locationLabel = UILabel()
            locationLabel.textColor = UIColor.app.gray.dynamic
            locationLabel.font = UIFont.systemFont(ofSize: 14)
            locationLabel.textAlignment = .left
            locationLabel.numberOfLines = 0
            self.card.addSubview(locationLabel)
            locationLabel.constrain(to: self.titleLabel).leadingTrailing().yAxis(.top, to: .bottom, constant: 4)
            locationLabel.constrain(to: self.card).bottom(constant: 12)
            
            statusBg = UIView()
            statusBg.layer.cornerRadius = 10
            self.card.addSubview(statusBg)
            statusBg.constrain(to: self.card).trailing(constant: 8).centerY().topBottom(.greaterThanOrEqual)
            
            statusLabel = UILabel(frame: .init(x: 0, y: 0, width: 80, height: 40))
            statusLabel.text = "Unknown"
            statusLabel.textColor = .app.pure_white.value
            statusLabel.font = .systemFont(ofSize: 12)
            self.statusBg.addSubview(statusLabel)
            statusLabel.constrain(to: self.statusBg).leadingTrailingTopBottom(constant: 12)
            
        }
        
        public func configure(with model: Model.order) {
            image.image = UIImage(named: model.product.image)
            titleLabel.text = model.product.name
            locationLabel.text = "lat: \(String(format: "%.2f", model.location.latitude)), long: \(String(format: "%.2f", model.location.longitude))"
            switch Model.order.status.init(rawValue: model.status) {
            case .pending:
                self.statusBg.backgroundColor = .app.gray.value
                self.statusLabel.text = "Pending"
            case .process:
                self.statusBg.backgroundColor = .app.yellow.value
                self.statusLabel.text = "In Process"
            case .delivery:
                self.statusBg.backgroundColor = .app.blue.value
                self.statusLabel.text = "Delivery"
            case .delivered:
                self.statusBg.backgroundColor = .app.green.value
                self.statusLabel.text = "Delivered"
            case .none:
                break
            }
        }
    }
    
}
